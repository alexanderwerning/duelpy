
Welcome to  the Duelpy documentation!
=====================================
Open-Source Python package for preference-based multi-armed Bandits algorithms.

For an overview of the problem and algorithms, see ~survey paper~.

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   autoapi/index
   installation
   glossary
   references


Indices and tables
==================

* :ref:`genindex`

Fork this project
==================

* https://gitlab.com/duelpy/duelpy