# Use the python image to speed up the setup.
# https://hub.docker.com/r/library/python/tags/
image: python:latest

# Configure pipeline to run on merged results.
# https://docs.gitlab.com/ee/ci/merge_request_pipelines/pipelines_for_merged_results
#
# This would avoid a pipeline failure after merge.
include:
  - template: 'Workflows/MergeRequest-Pipelines.gitlab-ci.yml'

stages:
  - test
  - deploy

# Change pip's cache directory to be inside the project directory since we can
# only cache local items.
variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

# Pip's cache doesn't store the python packages
# https://pip.pypa.io/en/stable/reference/pip_install/#caching
#
# If you want to also cache the installed packages, you have to install
# them in a virtualenv and cache it as well.
cache:
  paths:
    - .cache/pip
    - venv/

before_script:
  - apt-get update -qy
  # libopenblas-dev is necessary for scipy, and scipy is pulled in by our
  # seaborn dependency
  - apt-get install -y python3-dev python3-pip libopenblas-dev
  - python3 -V  # Print out python version for debugging
  - pip3 install virtualenv
  - virtualenv venv
  - source venv/bin/activate

lint:
  stage: test
  script:
  - pip install pre-commit
  - pre-commit run --all-files

pytest:
  stage: test
  script:
  - pip install pytest
  - pip install -r requirements.txt
  - pytest

doc_test_build:
  stage: test
  script:
  - pip install --upgrade sphinx
  - pip install sphinxcontrib-bibtex
  - pip install sphinx_rtd_theme
  - pip install sphinx-autoapi
  - pip install -r requirements.txt
  - sphinx-build -c sphinx -b html sphinx doc_test
  artifacts:
    expose_as: 'docs'
    paths:
      - doc_test
    expire_in: 1 week
  except:
  - master

doc_deploy_build:
  stage: deploy
  script:
  - pip install --upgrade sphinx
  - pip install sphinxcontrib-bibtex
  - pip install sphinx_rtd_theme
  - pip install sphinx-autoapi
  - pip install -r requirements.txt
  - sphinx-build -c sphinx -b html sphinx public
  artifacts:
    paths:
    - public
  only:
  - master

pypi:
  stage: deploy
  script:
  - pip install twine setuptools wheel
  - python3 setup.py sdist bdist_wheel
  # The API token is provided in the TWINE_PASSWORD environment variable configured in the CI
  - twine upload --user __token__ dist/*
  only:
    # tags of type v1.2.3
    - /^v(\d+\.)?(\d+\.)?(\d+)$/
  except:
    - branches
